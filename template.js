module.exports.generic={	
		"attachment": {
			"type": "template",
			"payload": {
				"template_type": "generic",
				"elements": [{
					"title": "Welcome to QMART!",
					"image_url": "https://petersfancybrownhats.com/company_image.png",
					"subtitle": "I believe you are trying to pay your water bill.",
					"default_action": {
						"type": "web_url",
						"url": "https://petersfancybrownhats.com/view?item=103",
						"webview_height_ratio": "tall"
					},
					"buttons": [{
						"type": "web_url",
						"url": "https://petersfancybrownhats.com",
						"title": "Pay water"
					}, {
						"type": "postback",
						"title": "Chat with human",
						"payload": "DEVELOPER_DEFINED_PAYLOAD"
					}]
				}]
			}
		}	
};

module.exports.list= {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type": "list",
        "top_element_style": "compact",
        "elements": [
          {
            "title": "SECTION 1",
            "subtitle": "Section 1.1",
            "image_url": "https://www.google.com",          
            "buttons": [             
              {
                "title": "Section 1.2",
                "type": "postback",
                "payload": "DEVELOPER_DEFINED_PAYLOAD",
                //"url": "https://www.google.com",
                //"messenger_extensions": false,
                //"webview_height_ratio": "tall",
                         
              }
            ]
          },
          {
            "title": "SECTION 2",
            "subtitle": "Section 2.1",
            "default_action": {
              "type": "web_url",
              "url": "https://www.google.com",
              "messenger_extensions": false,
              "webview_height_ratio": "tall"
            }
          },
          {
            "title": "SECTION 3",
            "image_url": "https://peterssendreceiveapp.ngrok.io/img/blue-t-shirt.png",
            "subtitle": "section 3.1",
            "default_action": {
              "type": "web_url",
              "url": "https://peterssendreceiveapp.ngrok.io/view?item=101",
              "messenger_extensions": false,
              "webview_height_ratio": "tall",
              
            },
            "buttons": [
              {
                "title": "Section 4",
                "type": "web_url",
                "url": "https://peterssendreceiveapp.ngrok.io/shop?item=101",
                "messenger_extensions": false,
                "webview_height_ratio": "tall",
                           
              }
            ]        
          }
        ],
         "buttons": [
          {
            "title": "Ein?",
            "type": "postback",
            "payload": "payload"            
          }
        ]  
      }
    }
  }


module.exports.listbutton={
                "title": "Button LM",
                "type": "web_url",
                "url": "https://www.google.com",
                //"messenger_extensions": false,
                //"webview_height_ratio": "tall",
                         
              };